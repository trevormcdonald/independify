#!/bin/env/python

from gensim.models import KeyedVectors
import argparse

model = KeyedVectors.load_word2vec_format('wiki-news-300d-1M-subword.vec')

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--sent", required=True,
	help="sentence to independify")
args = vars(ap.parse_args())

sentence = args['sent'].split()

with open('usdeclar.txt', 'r') as the_declaration:
    lines = list(the_declaration.read().splitlines())
    #TODO: make it a triple list comprehension
    lines = [line for line in lines if line]
    words = list(set([filter(str.isalnum, word) for line in lines for word in line.split()]))
    translated_sentence = [model.most_similar_to_given(w, words[1:-1]) for w in sentence]
    print(translated_sentence)
