# independify
Translate your sentence with the Declaration of Independence.

## Install

* `pip install gensim`
* download the word vectors from [here](https://fasttext.cc/docs/en/english-vectors.html)

## Run

`python ind.py -s "your sentence here"`

Have a book handy, this one takes a bit.
