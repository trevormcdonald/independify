#!/bin/env/python

import tweepy
from secrets import *
import independify

print("loaded word vectors")

#create an OAuthHandler instance
# Twitter requires all requests to use OAuth for authentication
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)

auth.set_access_token(access_token, access_secret)


#Construct the API instance
api = tweepy.API(auth) # create an API object
print("authenticated")
def tweet_independence(text, username, status_id):
	api.update_status('@{0} '.format(username) +
						' '.join(independify.translate(text)),
				in_reply_to_status_id=status_id)




class BotStreamer(tweepy.StreamListener):

	def on_status(Self, status):
		print(status.text)
		username = status.user.screen_name
		status_id = status.id
		tweet_independence(status.text, username, status_id)



my_stream_listener = BotStreamer()
stream = tweepy.Stream(auth, my_stream_listener)
stream.filter(track=['@independify1'])
