#!/bin/env/python

from gensim.models import KeyedVectors

model = KeyedVectors.load_word2vec_format('../ind/wiki-news-300d-1M-subword.vec')

declaration = None

with open('usdeclar.txt', 'r') as the_declaration:
    lines = list(the_declaration.read().splitlines())
    #TODO: make it a triple list comprehension
    lines = [line for line in lines if line]
    declaration = list(set([filter(str.isalnum, word) for line in lines for word in line.split()]))

def translate(sentence):
	translated_sentence = [model.most_similar_to_given(w, declaration[1:-1]) for w in sentence.split() if w in model.vocab]
	return translated_sentence
